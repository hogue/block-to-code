Resultsdecision <- nrow(subset(Statvalstr, Prt4 == '&lt;' | Prt4 == '&gt;' | Prt5 == '&amp;' | Prt6 == '&quot;' | Prt6 == '&apos;' , select =c(Statvalnum, Altnum, Statvalcum2, Substr, Start, Prt4, Prt5, Prt6)))
Resultsdecisionalt <- data.frame(Resultsdecision)
Resultsdecision2 <- nrow(subset(Resultsdecisionalt, Resultsdecision > 0, select=c(Resultsdecision)))
Resultsdecisionval <- c(0, 1)
Resultsdecisionprog <- c('Subrangeoutput.R','Altrangesubstralt.R')
Decisionres <- data.frame(Resultsdecisionval, Resultsdecisionprog)
Decisionres$Resultsdecisionoutput <- Resultsdecision2
Resdecprog <- subset( Decisionres, Resultsdecisionval == Resultsdecisionoutput, select = c(Resultsdecisionprog)) 
Resdecoutprog <- paste0(Resdecprog$Resultsdecisionprog)
source(Resdecoutprog)
