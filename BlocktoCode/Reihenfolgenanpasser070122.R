Numcorr <- subset(Extraktres2, Corrvalue == -1, select=c(Num))
Numcorr$Corrval <- -1
Numcorr$Numnew <- Numcorr$Num + Numcorr$Corrval
Numcorr$Corrup <- 1
Numcorr2 <- c(Numcorr$Num, Numcorr$Numnew)
Corrval2 <- c(Numcorr$Corrval, Numcorr$Corrup)
Corrconst <- data.frame(Numcorr2, Corrval2)
Extraktres3 <- merge(Extraktres2, Corrconst, by.x="Num", by.y="Numcorr2", all.x=TRUE) 
Extraktres3[is.na(Extraktres3)] <- 0
Extraktres3$Newnum <- Extraktres3$Num + Extraktres3$Corrval2
Extraktres4 <- Extraktres3[order(Extraktres3$Newnum),]
