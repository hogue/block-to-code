#Basepath <- Path
Basepath <- setwd("/home/paws/BlocktoCode/")
String <- scan(paste0(Basepath, "Extractfolder/project.json"), what="raw")
String2 <- data.frame(String)
setwd(Path)
Collapsestring <- paste(String2$String, collapse=" ")
Collapsestring2 <- Collapsestring
Collapsestring <- rep(Collapsestring, nchar(Collapsestring))
Collapsestring <- data.frame(Collapsestring)
Collapsestring$Num <- 1
Collapsestring$Num <- cumsum(Collapsestring$Num)
Collapsestring$Substr <- substr(Collapsestring$Collapsestring, Collapsestring$Num, Collapsestring$Num + 27)
#Collapsestring$Substr3 <- substr(Collapsestring$Collapsestring, Collapsestring$Num, Collapsestring$Num + 31)
Collapsestring$Substr2 <- substr(Collapsestring$Collapsestring, Collapsestring$Num, Collapsestring$Num + 6)
Collapsestringsub <- subset(Collapsestring, Substr == ':{"opcode":"procedures_call"' , select=c(Substr, Num))
Collapsestringsub$Collapsestring2 <- Collapsestring2
Collapsestringsub$Identification <- substr(Collapsestringsub$Collapsestring2, Collapsestringsub$Num - 21, Collapsestringsub$Num - 2)
Collapsestringsub$Identificationfollow1 <- substr(Collapsestringsub$Collapsestring2, Collapsestringsub$Num + 36, Collapsestringsub$Num + 39)
Collapsestringsub$Identificationfollow2 <- substr(Collapsestringsub$Collapsestring2, Collapsestringsub$Num + 37, Collapsestringsub$Num + 56)
Collapsestringsub$Identificationfollowres <- ifelse(Collapsestringsub$Identificationfollow1 == 'null', '', Collapsestringsub$Identificationfollow2)
Collapsestringsub <- subset(Collapsestringsub, select=c(Substr, Num, Identification, Identificationfollowres)) 
Collapsestringsub2 <- subset(Collapsestring, Substr2 == '"warp":', select=c(Substr2, Num))
Collapsestringsubmin <- -1
Precollapsefurtherstringsentence <- 'Start'
Prevarextractres <- 'Start'
Prevarextractres2 <- 0
Prognum <- 1
ifelse(nrow(Collapsestringsub) < Prognum, '', source("Varextractresframesentence.R"))
Collapsestringsub$Identificationordernum <- 1
Collapsestringsub$Identificationordernum <- cumsum(Collapsestringsub$Identificationordernum)
Identificationorder <- subset(Collapsestringsub, Identificationfollowres == '', select=c(Identification, Identificationordernum))
Collapsestringsub$Identificationsearch <- Identificationorder$Identification
Preidentificationordernum <- 0
Identificationordernum <- c(Preidentificationordernum, Identificationorder$Identificationordernum)
Preidentificationordernum <- Identificationordernum
Prognum2 <- 2
ifelse(nrow(Collapsestringsub) < Prognum2, '', source("Identificationorderframecreator.R"))
Identificationordernum2 <- Identificationordernum[2:length(Identificationordernum)]
Identificationordernumspec <- length(Identificationordernum2):1
Identificationorderframe <- data.frame(Identificationordernum2, Identificationordernumspec)
Varextractres2 <- Varextractres2[2:length(Varextractres2)]
Varextractresnum2 <- Prevarextractres2[2:length(Prevarextractres2)]
Varextractresframe <- data.frame(Varextractres2, Varextractresnum2)
Varextractresframe$Posnum <- ifelse(Varextractresframe$Varextractresnum2 == 1, 1, 0)
Varextractresframe$Posnum <- cumsum(Varextractresframe$Posnum)
Varextractresframe <- merge(Varextractresframe, Identificationorderframe, by.x="Posnum", by.y="Identificationordernum2", all.x=TRUE)
Varextractresframe <- Varextractresframe[order(Varextractresframe$Identificationordernumspec),]
Varextractresframe$Identcalc <- Varextractresframe$Identificationordernumspec * 1000000 + Varextractresframe$Varextractresnum2
Varextractresframe <- Varextractresframe[order(Varextractresframe$Identcalc),]
Varextractresframe$Varextractres3 <- paste0('<l>',substr(Varextractresframe$Varextractres2, 2, nchar(Varextractresframe$Varextractres2) - 1),'</l>')
Varextractresframe$Length <- nchar(Varextractresframe$Varextractres3)
Varextractresframe$Lengthcum <- cumsum(Varextractresframe$Length)
Varextractresframe$Prelength <- Varextractresframe$Lengthcum - Varextractresframe$Length
Relprelength <- subset(Varextractresframe, Varextractresnum2 == 1, select=c(Varextractresnum2, Prelength, Identcalc))
Varextractstr <- paste(Varextractresframe$Varextractres3, collapse="")
Relprelength$Varextractstr <- Varextractstr
Relprelength$Start <- Relprelength$Prelength + 1
Relprelength$End <- c(Relprelength$Prelength[2:nrow(Relprelength)], nchar(Varextractstr))
Relprelength$Substr <- substr(Relprelength$Varextractstr, Relprelength$Start, Relprelength$End) 
Collapsefurtherstringsentence <- Collapsefurtherstringsentence[2:length(Collapsefurtherstringsentence)]
Sentenceframe <- data.frame(Collapsefurtherstringsentence)
Identificationorderframe <- Identificationorderframe[order(Identificationorderframe$Identificationordernum2),]
Sentenceframe$Identificationordernumspec <- Identificationorderframe$Identificationordernumspec
Sentenceframe <- Sentenceframe[order(Sentenceframe$Identificationordernumspec),]
Sentenceframe$Spec <- paste0('<custom-block s="', Sentenceframe$Collapsefurtherstringsentence, '">')
Sentenceframe$Spec2 <- Relprelength$Substr
Sentenceframe$Specres <- paste0(Sentenceframe$Spec, Sentenceframe$Spec2, '</custom-block>')
source("Codeoutputscript2.R")
print('Ergebnis in Collapsefurtherstringsentence, Collapsefurtherparts, Collapsefurtherpartsframe2, Varextractresframe')
